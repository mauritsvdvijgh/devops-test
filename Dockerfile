FROM debian:bullseye-20210511-slim AS build
ARG LITECOIN_VERSION="0.18.1"
ARG LITECOIN_BINARY_URL="https://download.litecoin.org/litecoin-$LITECOIN_VERSION/linux/litecoin-$LITECOIN_VERSION-x86_64-linux-gnu.tar.gz"
ARG LITECOIN_BINARY_SHA256="ca50936299e2c5a66b954c266dcaaeef9e91b2f5307069b9894048acf3eb5751"

ADD $LITECOIN_BINARY_URL /tmp/litecoin.tar.gz
RUN [ "$LITECOIN_BINARY_SHA256  /tmp/litecoin.tar.gz" = "$(sha256sum /tmp/litecoin.tar.gz)" ]
RUN tar -xf /tmp/litecoin.tar.gz litecoin-0.18.1/bin/litecoind -O > /usr/local/bin/litecoind \
    && chmod +x /usr/local/bin/litecoind


FROM debian:bullseye-20210511-slim AS runtime
RUN groupadd litecoin -g 1000 && useradd litecoin -u 1000 -g litecoin --create-home
USER litecoin
COPY --from=build /usr/local/bin/litecoind /usr/local/bin/litecoind

ENTRYPOINT ["litecoind"]
