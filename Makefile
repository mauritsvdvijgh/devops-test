.DEFAULT_GOAL := test

build: Dockerfile
	docker build -t mauritso/litecoin:0.18.1 .

test: build
	grype --fail-on low mauritso/litecoin:0.18.1

deploy-test:
	kubectl apply --validate --dry-run=client -f k8s/

