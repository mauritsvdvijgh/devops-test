import json
import sys

counts = {}
for line in sys.stdin:
    data = json.loads(line)
    for k, v in data.items():
        c = counts.get(k, 0)
        counts[k] = c + v

for k, c in sorted(counts.items()):
    print(f'{k}:{c}')
