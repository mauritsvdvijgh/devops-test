# General
This repo contains my answers to the DevOps Test.

## Repo layout
```
├── Dockerfile                                                          q1
├── Makefile                                                            n/a
├── README.md                                                           q1-5
├── k8s                                                                 q2
│   └── statefulset.yaml                                                q2
├── screens                                                             q1-3
│   ├── digitalocean-adding-new-nodes.png                               q3
│   ├── gitlab-ci-adduser-uid-issue.png                                 q3
│   ├── gitlab-ci-image-push-error-dockerio-notindexdockerio.png        q3
│   ├── gitlab-ci-kubectl-image-permission-denied.png                   q3
│   ├── gitlab-ci-missing-docker-service-error.png                      q3
│   ├── gitlab-ci-succesful-deployment.png                              q3
│   ├── gitlab-ci-variables.png                                         q3
│   ├── node-size-scheduling-issue-2.png                                q3
│   ├── node-size-scheduling-issue-3.png                                q3
│   ├── node-size-scheduling-issue.png                                  q3
│   ├── node-wait-until-ready.png                                       q3
│   ├── personal-server-image-test-run-logs.png                         q1
│   ├── personal-server-image-test-run-monitoring.png                   q1/2
│   ├── pod-logs.png                                                    q3
│   ├── pod-oomkilled.png                                               q3
│   └── pod-pv-permission-problem.png                                   q3
└── script-questions                    q4/5
    ├── category_count.py               q5
    └── category_counts_jsons.txt       q4/5
```

## Time spent
All in all I went a bit over the "few hours" in the end, in part due to encountering some new problems with the new setups/infra used for q3.

```
Wk  Date       Day Tags                                 Start      End    Time   Total
W22 2021-06-01 Tue q1, devops,                       23:10:07  0:00:00 0:49:53 0:49:53
W22 2021-06-02 Wed q1, devops,                        0:00:00  0:08:33 0:08:33 0:08:33
W22 2021-06-06 Sun devops,       ,                   15:10:00 15:21:39 0:11:39
                   devops,       , q2                15:21:39 16:20:07 0:58:28
                   devops,       , q3                16:20:07 16:36:18 0:16:11
                   devops,       , q3                18:08:19 18:52:59 0:44:40
                   devops,       , q3                20:00:03 21:30:00 1:29:57
                   devops,       , q45               21:30:00 22:30:00 1:00:00
                   devops, fixq3,                    22:35:43 23:00:00 0:24:17
                   devops, finishing-touches,        23:00:00 23:22:12 0:22:12 5:27:24

                                                                               6:25:50
```

# 1. Docker-ayes
- Litecoin version, url and binary hash given as build args so it can be substituted on a dime.
- Separate runtime image with only litecoind, of course the image can be modified to contain the other binaries
- Runs as litecoin user that is in the litecoin group
- Used entrypoint instead of command so you can directly pass the args to the default binary (litecoind), e.g.: `docker run -it mauritso/litecoin:0.18.1 --help`
Docker-ayes: Write a Dockerfile to run Litecoin 0.18.1 in a container. It should somehow verify the checksum of the downloaded release (there's no need to build the project), run as a normal user, and when run without any modifiers (i.e. docker run somerepo/litecoin:0.18.1) should run the daemon, and print its output to the console. The build should be security conscious (and ideally pass a container image security test such as Anchore).
- Default make command builds the image and runs `grype --fail-on low` on it so it fails if it finds a vulnerability with low or higher severity
- No EXPOSE set as it is just a hint and it is not clear what exactly the use case for this image is going to be (does it need to expose rpc/litecoin network port?)

# 2. k8s FTW
- Persistent volume of 120GB, current blockchain size is 42GB, should last a year, preferably have some monitoring to alert when usage hits 75%
- Requests and limits the same to get QoS Guaranteed.
- Limits based on running the container on my own server and monitoring the cpu/mem using portainer, recommendations online, noticing people run it on raspberry pis.
- From https://litecoin.info/index.php/Litecoin.conf, default db size 450mb, maxmempool 300mb, makes 750mb.
- Noticed shutdown can take a while thus set termination grace period to 60s

# 3. All the continuouses
- Ran into https://gitlab.com/gitlab-org/gitlab-runner/-/issues/26501, unfortunately still not possible to use an image without shell directly
- Deploys to digitalocean k8s cluster
- Only pushes latest/0.18.1 tag when security test passes
- Uses commit sha as image tag for deployment to ensure the statefulset changes/new images are pulled for every deployed commit
- Made sure uid/gid are forced to 1000/1000 in the Dockerfile so they can safely be used under securityContext in the statefulset definition

# 4. Script kiddies
So this is actually a problem I encountered this week which fits this question, the problem is as follows:

From logs of pods with a specific prefix we want total counts of some event per category, these pods have in their logs lines like this:
```
{"2":1}
{"3":2,"2":1}
{"2":1}
{"2":1}
{"2":1}
```
interspersed with other log lines. The key is the category, the value is the count assigned to that category. The goal was to get the totals per category.

1. Retrieve the logs: `kubectl get pods -o name | grep $POD_PREFIX | xargs -I{} kubectl logs --tail=-1 {} > logs.txt`
   Unfortunately they had no common label so could not use the kubectl label selector `-l`
2. Get the relevant lines `cat logs.txt | grep -E '[0-9]+":[0-9]+' > category_counts_jsons.txt`
   Useless use of cat? I like creating the pipelines from left to right for clarity/readability and my computer has room for another process.
3. Most quick and dirty version that is 90% of the solution of the problem with 10% of the effort:
```
$ cat category_counts_jsons.txt| sort | uniq -c
1288 {"2":1}
   2 {"2":2}
   1 {"3":19,"4":20}
  10 {"3":1}
   1 {"3":2,"2":1}
   1 {"3":2,"5":7}
   2 {"3":2}
   1 {"3":3}
   1 {"3":7}
```
4. After looking at unix tools to solve this for a few minutes I figured that writing a python script would be the fastest way to achieve this so chronologically its time to switch to the answer to question 5.
5. As a challenge and to answer this question I devised a way to solve this using awk, although jq might be better suited here:
```
# some use of tr to delete and replace characters gets us pretty close, but no group by like functionality just yet:
cat script-questions/category_counts_jsons.txt | tr -d '{"}' | tr ',' '\n' | sort | uniq -c
1289 2:1
   2 2:2
  10 3:1
   1 3:19
   4 3:2
   1 3:3
   1 3:7
   1 4:20
   1 5:7

# awk part inspired by https://stackoverflow.com/a/2613073
$ cat script-questions/category_counts_jsons.txt | tr -d '{"}' | tr ',' '\n' | awk 'BEGIN {FS=OFS=":"}{arr[$1]+=$2}END {for (i in arr) print i,arr[i]}'
2:1293
3:47
4:20
5:7
```

# 5. Script grown-ups
## Script
I wrote the following python script to quickly count everything up given `category_counts_jsons.txt` to stdin:
```
import json
import sys

counts = {}
for line in sys.stdin:
    data = json.loads(line)
    for k, v in data.items():
        c = counts.get(k, 0)
        counts[k] = c + v

print(counts)
```

## Result
```
$ cat script-questions/category_counts_jsons.txt | python3 script-questions/category_count.py
2:1293
3:47
4:20
5:7
```

## Test
Sanity check to at least check if I have made the same mistakes in both versions:
```
$ diff <(cat script-questions/category_counts_jsons.txt | tr -d '{"}' | tr ',' '\n' | awk 'BEGIN {FS=OFS=":"}{arr[$1]+=$2}END {for (i in arr) print i,arr[i]}') <(cat script-questions/category_counts_jsons.txt| python3 script-questions/category_count.py) && echo $?
0
```

# 6. Terraform lovers unite
Skipping this one as I have limited experience with Terraform and AWS IAM roles. Guessing it might take me up to 2 hours, so combined with the time invested in 1-5 I would get above "a few hours".

# 7. ?
Tangentially related to question 4, something I think is kind of neat. I use this shell function to log my time worked to paymo (project management/task/time tracker software).

Time warrior can export time entries as json so it uses sed to replace some keys by the ones the paymo API expects and it then uses jq to:
- Make sure the tags I assign to the tasks appear comma separated in the description of the paymo task
- Assign a valid task id, to manually adjust into the right one afterwards
- Delete some keys the paymo API does not expect
- Convert to json with a fix for a problem that I admittedly should have added a comment for.

After modifications it uses xargs -n 1 to submit these one by one to the paymo API using curl.

```
# open paymo and create the time entries from timewarrior (CLI time keeping tool) in paymo
standup() {
    paymo
    timew export elnino "${1:-:yesterday}" \
        | sed -e 's/start/start_time/g' -e 's/end/end_time/g' \
        | jq -cj '.[]
            | .description = (.tags | join(","))
            | del(.id)
            | del(.tags)
            | .task_id=5450652 # smoke break task ID, need to manually assign the right paymo tasks after running this
            | tojson+"\u0000"' \
        | xargs -0 -n1 curl -H 'Accept: application/json' -H 'Content-Type: application/json' -u $PAYMO_API_KEY:X https://app.paymoapp.com/api/entries -d
}
```

